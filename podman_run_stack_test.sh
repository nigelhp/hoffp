#!/usr/bin/sh

# NOTE: container runs as 'root' to have access to mapped volume, but as we are running
#       rootless podman, the process will be run as the unprivileged host user
# NOTE: uses :Z in volume mapping to label the volume as 'private unshared' for SELinux

podman run --rm --user root -v "$PWD":/usr/src/hoffp:Z -w /usr/src/hoffp hoffp:latest stack test 
