# hoffp

An exploration of the ideas presented in the paper:

__Hutton, G., (1992), "Higher-Order Functions for Parsing", Journal of Functional Programming, July 1992__

Having implemented the parser library, it is then put to use attempting the following katas:
* Roy Osherove's [String Calculator](https://osherove.com/tdd-kata-1)
* Emmanuel Gaillot & Christophe Thibaut's [Bank OCR](https://codingdojo.org/kata/BankOCR/)<sup>1<sup>

<sub>1<sub>Only the initial parsing related stories are implemented here


### Development Tooling

Build the project: ```stack build```

Lint the project: ```stack exec -- hlint -g```

#### Testing

Run the unit tests and exit: ```stack test```

Watch the project, running the unit tests on change: ```stack exec -- sensei -isrc -itest test/Spec.hs```