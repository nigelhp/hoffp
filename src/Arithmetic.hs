module Arithmetic (Expr (..), expn, factor, term) where

import Parser (Parser)
import Parser.Combinator (alt, thenp)
import Parser.Manipulation (number, thenx, using, xthen)
import Parser.Primitive (literal)

data Expr
  = Num Integer
  | Mul Expr Expr
  | Div Expr Expr
  | Add Expr Expr
  | Sub Expr Expr
  deriving (Eq, Show)

{-
  expn   ::= term + term | term - term | term
  term   ::= factor * factor | factor / factor | factor
  factor ::= digit+ | (expn)
-}

expn :: Parser Char Expr
expn =
  ((term `thenp` (literal '+' `xthen` term)) `using` plus)
    `alt` ((term `thenp` (literal '-' `xthen` term)) `using` minus)
    `alt` term
  where
    plus (x, y) = x `Add` y
    minus (x, y) = x `Sub` y

term :: Parser Char Expr
term =
  ((factor `thenp` (literal '*' `xthen` factor)) `using` times)
    `alt` ((factor `thenp` (literal '/' `xthen` factor)) `using` divide)
    `alt` factor
  where
    times (x, y) = x `Mul` y
    divide (x, y) = x `Div` y

factor :: Parser Char Expr
factor =
  (number `using` value)
    `alt` (literal '(' `xthen` expn `thenx` literal ')')
  where
    value digits = Num (read digits)
