module StringCalculator (add, StringCalculatorError (..)) where

import Parser (Parser)
import Parser.Combinator (alt, thenp)
import Parser.FreeFormat (anyp)
import Parser.Manipulation (into, many, number, some, string, thenx, using, xthen)
import Parser.Primitive (failp, literal, satisfy, succeedp)

data StringCalculatorError
  = ParseError
  | NegativesNotAllowedError [Integer]
  deriving (Eq, Show)

add :: String -> Either StringCalculatorError Integer
add s = case customDelimiters s of
  (delimiters, unconsumed) : _ -> eval (numbersDelimitedBy $ anyp string delimiters) unconsumed
  [] -> eval (numbersDelimitedBy aDefaultDelimiter) s

-- treat any unconsumed input as a parse error
eval :: Parser Char [Integer] -> String -> Either StringCalculatorError Integer
eval p s = case p s of
  [] -> Left ParseError
  (_, _ : _) : _ -> Left ParseError
  (numbers, []) : _ ->
    case filter isNegative numbers of
      [] -> Right (sum $ filter isInRange numbers)
      negatives -> Left (NegativesNotAllowedError negatives)
    where
      isNegative = (< 0)
      isInRange = (<= 1000)

--
-- //<delimiter>\n                     - defines a single character custom delimiter
-- //[<delimiter>]\n                   - defines a multi character custom delimiter
-- //[<delimiter1>][<delimiter2>]\n    - defines many multi character custom delimiters
--                                     - (note assumption that a multi character delimiter cannot itself contain ']' as this signifies the end of delimiter)
customDelimiters :: Parser Char [String]
customDelimiters = prefix `xthen` (singleCharDelimiter `alt` some multiCharDelimiter) `thenx` newline
  where
    singleCharDelimiter = anyChar `using` \c -> [[c]]
    multiCharDelimiter = stringSurroundedBy '[' ']'
    prefix = literal '/' `thenp` literal '/'

{-
  When the end delimiter is part of a chain of consecutive occurrences of that delimiter we are "greedy" and
  continue up until the last instance.
  This allows the resulting string to contain start & end delimiter characters.

  Note that when there is no other character between start & end, the string itself must be comprised of a
  number of occurrences of end.
 -}
stringSurroundedBy :: Char -> Char -> Parser Char String
stringSurroundedBy start end = literal start `xthen` oneOrMoreCharsFollowedByEnd
  where
    oneOrMoreCharsFollowedByEnd = many (satisfy (/= end)) `into` prefixFollowedByEnd
    prefixFollowedByEnd [] = literal end `xthen` consecutiveEnds -- two or more
    prefixFollowedByEnd prefix = consecutiveEnds `using` \ends -> prefix ++ init ends
    consecutiveEnds = some (literal end)

aDefaultDelimiter :: Parser Char String
aDefaultDelimiter = delimiter `using` (: [])
  where
    delimiter = literal ',' `alt` newline

-- given a Parser for the delimiter, returns a Parser for a sequence of delimited numbers
numbersDelimitedBy :: Parser Char String -> Parser Char [Integer]
numbersDelimitedBy delimiter = oneOrMoreNum `alt` empty []
  where
    -- any sequence with a trailing delimiter must be followed by a single number
    oneOrMoreNum = (zeroOrMoreNum `thenp` num) `using` \(ns, n) -> ns ++ [n]
    zeroOrMoreNum = many (num `thenx` delimiter)

-- a Parser that consumes a sequence of digits optionally preceeded by a minus sign
num :: Parser Char Integer
num = (optional (literal '-') `thenp` number) `using` toNum
  where
    toNum (sign, digits) = read (sign ++ digits)

-- a Parser that either succeeds with a single value or no value
optional :: Parser Char a -> Parser Char [a]
optional p = (p `using` (: [])) `alt` succeedp []

-- a Parser that succeeds when the input is empty
empty :: a -> Parser Char a
empty a [] = succeedp a []
empty _ input = failp input

-- a Parser that accepts anything
anyChar :: Parser Char Char
anyChar = satisfy (const True)

-- a Parser for newline
newline :: Parser Char Char
newline = literal '\n'