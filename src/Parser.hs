module Parser (Parser) where


{-
  * Empty list denotes failure
  * Singleton list [(v,xs)] indicates success, with:
    - value v and unconsumed input xs
-}
type Parser a b = [a] -> [(b, [a])]