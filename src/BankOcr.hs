module BankOcr (AccountNumberEntry, Cell (..), Digit (..), OcrEntry (..), PossibleDigit (..), accountNumberEntry, blankLine, eval, ocrEntry, validateChecksum) where

import Data.Maybe (fromMaybe)
import Parser (Parser)
import Parser.Combinator (alt, thenp)
import Parser.Manipulation (into, returnp, thenx, using)
import Parser.Primitive (failp, literal, succeedp)

type Triple a = (a, a, a {- 3 parts -})

type Nonuple a = (a, a, a, a, a, a, a, a, a {- 9 parts -})

{-
 - Account Number Domain
 -}

-- Account Number Entry
data AccountNumberEntry
  = AccountNumber (Nonuple Digit)
  | BadChecksum (Nonuple Digit)
  | Illegible (Nonuple PossibleDigit)
  deriving (Eq)

instance Show AccountNumberEntry where
  show (AccountNumber digits) = showNonuple digits
  show (BadChecksum digits) = showNonuple digits ++ " ERR"
  show (Illegible possibleDigits) = showNonuple possibleDigits ++ " ILL"

accountNumberEntry :: Parser Char AccountNumberEntry
accountNumberEntry = ocrEntry `using` validateChecksum

validateChecksum :: OcrEntry -> AccountNumberEntry
validateChecksum (IllegibleEntry possibleDigits) = Illegible possibleDigits
validateChecksum (LegibleEntry digits@(d9, d8, d7, d6, d5, d4, d3, d2, d1)) =
  let multipliers = [9, 8 .. 1]
      digitValues = map digitToInt [d9, d8, d7, d6, d5, d4, d3, d2, d1]
      sumOfProducts = sum $ zipWith (*) multipliers digitValues
   in case sumOfProducts `mod` 11 of
        0 -> AccountNumber digits
        _ -> BadChecksum digits

-- Digit
data Digit
  = One
  | Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine
  | Zero
  deriving (Eq)

instance Show Digit where
  show = show . digitToInt

digitToInt :: Digit -> Int
digitToInt One = 1
digitToInt Two = 2
digitToInt Three = 3
digitToInt Four = 4
digitToInt Five = 5
digitToInt Six = 6
digitToInt Seven = 7
digitToInt Eight = 8
digitToInt Nine = 9
digitToInt Zero = 0

-- Possible Digit
data PossibleDigit
  = LegibleDigit Digit
  | IllegibleDigit DigitBlock
  deriving (Eq)

instance Show PossibleDigit where
  show (LegibleDigit digit) = show digit
  show (IllegibleDigit _) = "?"

{-
 - OCR Domain
 -}

-- OCR Entry
data OcrEntry
  = LegibleEntry (Nonuple Digit)
  | IllegibleEntry (Nonuple PossibleDigit)
  deriving (Eq)

instance Show OcrEntry where
  show (LegibleEntry digits) = showNonuple digits
  show (IllegibleEntry possibleDigits) = showNonuple possibleDigits

ocrEntry :: Parser Char OcrEntry
ocrEntry = ocrBlock `using` toOcrEntry
  where
    ocrBlock = cellLine `thenp` cellLine `thenp` cellLine `using` (transpose . flatten3)
    cellLine = ocrLine cell

-- Cell and combinations of
data Cell
  = Empty
  | Vertical
  | Horizontal
  deriving (Eq)

type DigitRow = Triple Cell

type DigitBlock = Triple DigitRow

type AccountNumberBlock = Nonuple DigitBlock

{-
 - Translations from parse results to business domain
 -}

toOcrEntry :: AccountNumberBlock -> OcrEntry
toOcrEntry (d9, d8, d7, d6, d5, d4, d3, d2, d1) = fromMaybe toIllegibleEntry toLegibleEntry
  where
    toIllegibleEntry = IllegibleEntry . toNonuple $ map toPossibleDigit digitBlocks
    toLegibleEntry = LegibleEntry . toNonuple <$> traverse toDigit digitBlocks
    digitBlocks = [d9, d8, d7, d6, d5, d4, d3, d2, d1]

toPossibleDigit :: DigitBlock -> PossibleDigit
toPossibleDigit cells = maybe (IllegibleDigit cells) LegibleDigit (toDigit cells)

toDigit :: DigitBlock -> Maybe Digit
toDigit cells
  | cells == one = Just One
  | cells == two = Just Two
  | cells == three = Just Three
  | cells == four = Just Four
  | cells == five = Just Five
  | cells == six = Just Six
  | cells == seven = Just Seven
  | cells == eight = Just Eight
  | cells == nine = Just Nine
  | cells == zero = Just Zero
  | otherwise = Nothing

one :: DigitBlock
one = ((Empty, Empty, Empty), (Empty, Empty, Vertical), (Empty, Empty, Vertical))

two :: DigitBlock
two = ((Empty, Horizontal, Empty), (Empty, Horizontal, Vertical), (Vertical, Horizontal, Empty))

three :: DigitBlock
three = ((Empty, Horizontal, Empty), (Empty, Horizontal, Vertical), (Empty, Horizontal, Vertical))

four :: DigitBlock
four = ((Empty, Empty, Empty), (Vertical, Horizontal, Vertical), (Empty, Empty, Vertical))

five :: DigitBlock
five = ((Empty, Horizontal, Empty), (Vertical, Horizontal, Empty), (Empty, Horizontal, Vertical))

six :: DigitBlock
six = ((Empty, Horizontal, Empty), (Vertical, Horizontal, Empty), (Vertical, Horizontal, Vertical))

seven :: DigitBlock
seven = ((Empty, Horizontal, Empty), (Empty, Empty, Vertical), (Empty, Empty, Vertical))

eight :: DigitBlock
eight = ((Empty, Horizontal, Empty), (Vertical, Horizontal, Vertical), (Vertical, Horizontal, Vertical))

nine :: DigitBlock
nine = ((Empty, Horizontal, Empty), (Vertical, Horizontal, Vertical), (Empty, Horizontal, Vertical))

zero :: DigitBlock
zero = ((Empty, Horizontal, Empty), (Vertical, Empty, Vertical), (Vertical, Horizontal, Vertical))

{-
 - Primitive parsers for the OCR format
 -}

blankLine :: Parser Char ()
blankLine = returnp (ocrLine empty) ()

ocrLine :: Parser Char Cell -> Parser Char (Nonuple DigitRow)
ocrLine pCell =
  ( digitRow
      `thenp` digitRow
      `thenp` digitRow
      `thenp` digitRow
      `thenp` digitRow
      `thenp` digitRow
      `thenp` digitRow
      `thenp` digitRow
      `thenp` digitRow `thenx` newline
  )
    `using` flatten9
  where
    digitRow = (pCell `thenp` pCell `thenp` pCell) `using` flatten3

cell :: Parser Char Cell
cell = empty `alt` vertical `alt` horizontal

empty :: Parser Char Cell
empty = literalCell ' ' Empty

vertical :: Parser Char Cell
vertical = literalCell '|' Vertical

horizontal :: Parser Char Cell
horizontal = literalCell '_' Horizontal

literalCell :: Char -> Cell -> Parser Char Cell
literalCell char cell = literal char `using` const cell

newline :: Parser Char Char
newline = literal '\n'

{-
 - Parser evaluation
 -}

-- treats any unconsumed input as a parse error
eval :: Parser Char a -> String -> Maybe a
eval p s = case p s of
  (x, []) : _ -> Just x
  (_x, _) : _ -> Nothing
  [] -> Nothing

{-
 - Helpers
 -}

transpose :: Triple (Nonuple a) -> Nonuple (Triple a)
transpose
  ( (x0y0, x1y0, x2y0, x3y0, x4y0, x5y0, x6y0, x7y0, x8y0),
    (x0y1, x1y1, x2y1, x3y1, x4y1, x5y1, x6y1, x7y1, x8y1),
    (x0y2, x1y2, x2y2, x3y2, x4y2, x5y2, x6y2, x7y2, x8y2)
    ) =
    ( (x0y0, x0y1, x0y2),
      (x1y0, x1y1, x1y2),
      (x2y0, x2y1, x2y2),
      (x3y0, x3y1, x3y2),
      (x4y0, x4y1, x4y2),
      (x5y0, x5y1, x5y2),
      (x6y0, x6y1, x6y2),
      (x7y0, x7y1, x7y2),
      (x8y0, x8y1, x8y2)
    )

flatten3 :: ((a, a), a) -> Triple a
flatten3 ((a, b), c) = (a, b, c)

flatten9 :: ((((((((a, a), a), a), a), a), a), a), a) -> Nonuple a
flatten9 ((((((((a, b), c), d), e), f), g), h), i) = (a, b, c, d, e, f, g, h, i)

showNonuple :: (Show a) => Nonuple a -> String
showNonuple (a, b, c, d, e, f, g, h, i) = concatMap show [a, b, c, d, e, f, g, h, i]

toNonuple :: [a] -> Nonuple a
toNonuple [a9, a8, a7, a6, a5, a4, a3, a2, a1] = (a9, a8, a7, a6, a5, a4, a3, a2, a1)
toNonuple _ = error "list of 9 elements required"
