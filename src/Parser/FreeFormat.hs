module Parser.FreeFormat (anyp, nibble, symbol) where

import Parser (Parser)
import Parser.Combinator (alt)
import Parser.Manipulation (many, string, thenx, xthen)
import Parser.Primitive (failp, literal)

{-
  The parser (nibble p) has the same behaviour as the parser p,
  except that it eats up any white-space in the input string before
  or afterwards
 -}
nibble :: Parser Char a -> Parser Char a
nibble p = white `xthen` p `thenx` white
  where
    white = many (anyp literal " \t\n")

-- white = many (literal ' ' `alt` literal '\t' `alt` literal '\n')

{-
 - Renamed from any to avoid clash with Prelude.any
 -}
anyp :: (a -> Parser b c) -> [a] -> Parser b c
anyp p = foldr (alt . p) failp

--anyp p = foldr (\a b -> b `alt` p a) failp

symbol :: [Char] -> Parser Char [Char]
symbol = nibble . string