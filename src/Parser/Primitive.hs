module Parser.Primitive (succeedp, failp, satisfy, literal) where

import Parser ( Parser )


{-
  Always succeeds, without consuming any of the input
  Renamed from succeed to match the opposing case of failp
-}
-- succeedp :: b -> [a] -> [(b, [a])]
succeedp :: b -> Parser a b
succeedp v inp = [(v, inp)]
-- succeedp v = \inp -> [(v, inp)]


{-
  Always fails, regardless of the input
  Renamed from fail to avoid conflict with Prelude.fail
-}
-- failp :: [a] -> [(b, [a])]
failp :: Parser a b
failp _ = []


{-
  Recognise a single symbol
-}
-- satisfy :: (a -> Bool) -> [a] -> [(a, [a])]
satisfy :: (a -> Bool) -> Parser a a
satisfy p [] = failp []
satisfy p (x:xs) | p x       = succeedp x xs
                 | otherwise = failp xs

-- literal :: Eq a => a -> [a] -> [(a, [a])]
literal :: Eq a => a -> Parser a a
literal x = satisfy (== x)