module Parser.Manipulation (into, many, number, returnp, some, string, thenx, using, word, xthen) where

import Data.Bitraversable (Bitraversable)
import qualified Data.Char as Char
import Parser (Parser)
import Parser.Combinator (alt, thenp)
import Parser.Primitive (literal, satisfy, succeedp)

{-
  Map
-}
using :: Parser a b -> (b -> c) -> Parser a c
(p `using` f) inp = [(f v, out) | (v, out) <- p inp]

{-
  flatMap

  This is not actually introduced in the 'manipulating values' section of the paper (2.3),
  but rather is defined in the 'result values' section (5.4).
  Note that in this case we discard all but the first success result from the original parser (p)
-}
into :: Parser a b -> (b -> Parser a c) -> Parser a c
(p `into` f) inp =
  case p inp of
    [] -> []
    (v, inp') : _ -> f v inp'

{-
  Repetition: one or more
-}
some :: Parser a b -> Parser a [b]
some p = (p `thenp` many p) `using` cons

number :: Parser Char [Char]
number = some (satisfy Char.isDigit)

word :: Parser Char String
word = some (satisfy Char.isLetter)

{-
  Repetition: zero or more

  many p = ((p `thenp` many p) `using` cons) `alt` succeedp []
-}
many :: Parser a b -> Parser a [b]
many p = some p `alt` succeedp []

{-
  Generalisation of literal from one symbol to a string of symbols
 -}
string :: Eq a => [a] -> Parser a [a]
string [] = succeedp []
string (x : xs) = (literal x `thenp` string xs) `using` cons

cons :: (a, [a]) -> [a]
cons = uncurry (:)

{-
  Special versions of `thenp`, which throw away either the left or right
  result values, as reflected by the position of the letter 'x' in their
  names
 -}
xthen :: Parser a b -> Parser a c -> Parser a c
p1 `xthen` p2 = (p1 `thenp` p2) `using` snd

thenx :: Parser a b -> Parser a c -> Parser a b
p1 `thenx` p2 = (p1 `thenp` p2) `using` fst

{-
  Sometimes we are not interested in the result from a parser at all,
  only that the parser succeeds.
  The parser (p `return` v) has the same behaviour as p, except that it
  returns v if successful.

  Renamed from return to avoid clash with the Prelude function
-}
returnp :: Parser a b -> c -> Parser a c
returnp p v = p `using` const v
