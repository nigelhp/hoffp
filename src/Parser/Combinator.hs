module Parser.Combinator (alt, thenp) where

import Parser (Parser)


{-
  Alternation  
  The parser (p1 `alt` p2) recognises anything that either p1 or p2 would.
  
  Note the use of inclusive either - it is acceptable for both parsers to
  succeed, in which case we return both results.
-}
alt :: Parser a b -> Parser a b -> Parser a b
(p1 `alt` p2) inp = p1 inp ++ p2 inp


{-
  Sequencing
  The parser (p1 `then` p2) recognises anything that p1 and p2 would if placed
  in succession.
  
  Renamed from then to avoid clash with the Haskell keyword  
-}
thenp :: Parser a b -> Parser a c -> Parser a (b,c)
(p1 `thenp` p2) inp = [((v1, v2), out2) | (v1, out1) <- p1 inp,
                                          (v2, out2) <- p2 out1]
