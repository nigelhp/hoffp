module Parser.FreeFormatSpec (spec) where

import Parser.FreeFormat (nibble)
import Parser.Manipulation (number)
import Test.Hspec (Spec, describe, it, shouldBe)

spec :: Spec
spec = do
  describe "nibble" $ do
    it "should return a parser that succeeds when the input parser succeeds and the input does not contain whitespace" $
      let parse = nibble number
       in parse "123" `shouldBe` [("123", ""), ("12", "3"), ("1", "23")]

    it "should return a parser that succeeds when the input parser succeeds after eating leading whitespace" $
      let parse = nibble number
       in parse " \t\n123" `shouldBe` [("123", ""), ("12", "3"), ("1", "23")]

    it "should return a parser that succeeds when the input parser succeeds after eating trailing whitespace" $
      let parse = nibble number
       in parse "123 \t\n" `shouldBe` [("123", ""), ("123", "\n"), ("123", "\t\n"), ("123", " \t\n"), ("12", "3 \t\n"), ("1", "23 \t\n")]

    it "should return a parser that succeeds when the input parser succeeds after eating both leading and trailing whitespace" $
      let parse = nibble number
       in parse " \t\n123 \t\n" `shouldBe` [("123", ""), ("123", "\n"), ("123", "\t\n"), ("123", " \t\n"), ("12", "3 \t\n"), ("1", "23 \t\n")]

    it "should return a parser that fails when the input parser fails and the input does not contain whitespace" $
      let parse = nibble number
       in parse "abc" `shouldBe` []