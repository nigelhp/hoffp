module Parser.ManipulationSpec (spec) where

import Data.Char
import qualified Data.Char as Char
import Parser (Parser)
import Parser.Combinator (alt)
import Parser.Manipulation (many, number, returnp, some, string, thenx, using, word, xthen)
import Parser.Primitive (failp, literal, succeedp)
import Test.Hspec (Spec, context, describe, it, shouldBe)

spec :: Spec
spec = do
  describe "using" $ do
    it "should return a parser that applies the supplied function to the result on parse success" $
      let input = "abc"
          parse = succeedp 42 `using` \n -> n + 1
       in parse input `shouldBe` [(43, input)]

    it "should return a parser that fails when the input parser fails" $
      let parse = failp `using` Char.toUpper
       in parse "abc" `shouldBe` []

    context "when the parser succeeds with multiple results" $ do
      it "should apply the supplied function to each result" $
        let input = "abc"
            parse = (succeedp 10 `alt` succeedp 20) `using` \n -> n + 1
         in parse input `shouldBe` [(11, input), (21, input)]

  describe "many" $ do
    it "should return a parser that succeeds with zero results when the input parser fails" $
      let input = "abc"
          parse = many (failp :: Parser Char Char)
       in parse input `shouldBe` [([], input)]

    it "should return a parser that succeeds with the results from both one and zero matches when the input parser succeeds once" $
      let parse = many (literal 'a')
       in parse "abc" `shouldBe` [("a", "bc"), ("", "abc")]

    it "should return a parser that succeeds with the results from two, one, and zero matches when the input parser succeeds twice" $
      let parse = many (literal 'a')
       in parse "aabc" `shouldBe` [("aa", "bc"), ("a", "abc"), ("", "aabc")]

    -- example from the paper
    it "applying the parser many (literal 'a') to the string \"aaab\" gives the list [(\"aaa\",\"b\"),(\"aa\",\"ab\"),(\"a\",\"aab\"),(\"\",\"aaab\")]" $
      let parse = many (literal 'a')
       in parse "aaab" `shouldBe` [("aaa", "b"), ("aa", "ab"), ("a", "aab"), ("", "aaab")]

  describe "some" $ do
    it "should return a parser that fails when the input parser fails" $
      let parse = some (failp :: Parser Char Char)
       in parse "abc" `shouldBe` []

    it "should return a parser that succeeds with a single result when the input parser succeeds once" $
      let parse = some (literal 'a')
       in parse "abc" `shouldBe` [("a", "bc")]

    it "should return a parser that succeeds with the results from both two and one matches when the input parser succeeds twice" $
      let parse = some (literal 'a')
       in parse "aabc" `shouldBe` [("aa", "bc"), ("a", "abc")]

    it "should return a parser that succeeds with the results from three, two, and one matches when the input parser succeeds three times" $
      let parse = some (literal 'a')
       in parse "aaab" `shouldBe` [("aaa", "b"), ("aa", "ab"), ("a", "aab")]

  describe "number" $ do
    it "should fail when the input does not start with a digit" $
      number "a123" `shouldBe` []

    it "should succeed with the first digit when the input starts with a single digit" $
      number "1abc" `shouldBe` [("1", "abc")]

    it "should succeed with the results from both two and one matches when the input starts with two digits" $
      number "12abc" `shouldBe` [("12", "abc"), ("1", "2abc")]

  describe "word" $ do
    it "should fail when the input does not start with a letter" $
      word "1abc" `shouldBe` []

    it "should succeed with the first letter when the input starts with an uppercase letter" $
      word "A123" `shouldBe` [("A", "123")]

    it "should succeed with the first letter when the input starts with a lowercase letter" $
      word "a123" `shouldBe` [("a", "123")]

    it "should succeed with the results from both two and one matches when the input starts with two letters" $
      word "aB1" `shouldBe` [("aB", "1"), ("a", "B1")]

    it "should succeed with the results combining the letter matches up to the first space" $
      word "one two" `shouldBe` [("one", " two"), ("on", "e two"), ("o", "ne two")]

  describe "string" $ do
    it "an empty string argument should return a parser that always succeeds" $ do
      let parse = string ""
      parse "" `shouldBe` [("", [])]
      parse "abc" `shouldBe` [("", "abc")]

    it "should return a parser that succeeds when the input string begins with the argument" $
      let parse = string "abc"
       in parse "abcde" `shouldBe` [("abc", "de")]

    it "should return a parser that fails when the input string does not begin with the argument" $
      let parse = string "abc"
       in parse "abx" `shouldBe` []

    -- example from the paper
    it "applying the parser string \"begin\" to the string \"begin end\" gives the output [(\"begin\", \" end\")]" $
      let parse = string "begin"
       in parse "begin end" `shouldBe` [("begin", " end")]

  describe "xthen" $ do
    it "should return a parser that drops the return value from the first parser when both parsers succeed" $
      let p1Value = 1
          p2Value = 2
          input = "abc"
          parse = succeedp p1Value `xthen` succeedp p2Value
       in parse input `shouldBe` [(p2Value, input)]

    it "should fail when the first parser succeeds but the second parser fails" $
      let parse = succeedp 42 `xthen` (failp :: Parser Char Char)
       in parse "abc" `shouldBe` []

    it "should fail when the first parser fails even though the second parser would succeed" $
      let parse = (failp :: Parser Char Char) `xthen` succeedp 42
      in parse "abc" `shouldBe` []

  describe "thenx" $ do
    it "should return a parser that drops the return value from the second parser when both parsers succeed" $
      let p1Value = 1
          p2Value = 2
          input = "abc"
          parse = succeedp p1Value `thenx` succeedp p2Value
       in parse input `shouldBe` [(p1Value, input)]

    it "should fail when the first parser succeeds but the second parser fails" $
      let parse = succeedp 42 `thenx` (failp :: Parser Char Char)
       in parse "abc" `shouldBe` []

    it "should fail when the first parser fails even though the second parser would succeed" $
      let parse = (failp :: Parser Char Char) `thenx` succeedp 42
      in parse "abc" `shouldBe` []

  describe "returnp" $ do
    it "returns a parser that returns the supplied value when the supplied parser succeeds" $
      let input = "abc"
          parse = succeedp 42 `returnp` "xyz"
       in parse input `shouldBe` [("xyz", input)]

    it "returns a parser that fails when the supplied parser fails" $
      let parse = failp `returnp` "xyz"
       in parse "abc" `shouldBe` []