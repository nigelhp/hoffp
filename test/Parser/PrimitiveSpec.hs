module Parser.PrimitiveSpec (spec) where

import Test.Hspec ( describe, it, shouldBe, Spec )
import Parser ( Parser )
import Parser.Primitive ( failp, succeedp, satisfy, literal )


spec :: Spec
spec = do
  describe "succeedp" $ do
    it "should return a parser that succeeds without consuming any of the input" $
      let value = 1
          parse = succeedp value
      in parse "abc" `shouldBe` [(1, "abc")]
 
    it "should return a parser that succeeds even when the input is empty" $
      let value = 2
          parse = succeedp value
      in parse "" `shouldBe` [(value, "")]


  describe "failp" $ do
    it "should be a parser that fails even when the input is non-empty" $
      let parse = failp :: Parser Char Char
      in parse "abc" `shouldBe` []

    it "should be a parser that fails when the input is empty" $
      let parse = failp :: Parser Char Char
      in parse "" `shouldBe` []


  describe "satisfy" $ do
    it "should return a parser that succeeds when the head of the input satisfies the predicate" $
      let parse = satisfy (const True)
      in parse "abc" `shouldBe` [('a', "bc")]

    it "should return a parser that fails when the head of the input does not satisfy the predicate" $
      let parse = satisfy (const False)
      in parse "abc" `shouldBe` []

    it "should return a parser that fails when the input is empty" $
      let parse = satisfy (const True)
      in parse "" `shouldBe` []


  describe "literal" $ do
    it "should return a parser that succeeds when the head of the input is the target symbol" $
      let parse = literal 'a'
      in parse "abc" `shouldBe` [('a', "bc")]

    it "should return a parser that fails when the head of the input is not the target symbol" $
      let parse = literal 'a'
      in parse "bcd" `shouldBe` []
    
    it "should return a parser that fails when the input is empty" $
      let parse = literal 'a'
      in parse "" `shouldBe` []

    -- example given in the paper
    it "applying the parser (literal '3') to the string \"345\" gives the result [('3',\"45\")]" $
      let parse = literal '3'
      in parse "345" `shouldBe` [('3', "45")]
