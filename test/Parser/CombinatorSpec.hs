module Parser.CombinatorSpec (spec) where

import Parser (Parser)
import Parser.Combinator (alt, thenp)
import Parser.Primitive (failp, literal, succeedp)
import Test.Hspec (Spec, context, describe, it, shouldBe)

spec :: Spec
spec = do
  describe "alt" $ do
    it "should return success when the first parser succeeds and the second fails" $
      let value = 42
          input = "abc"
          parse = succeedp value `alt` failp
       in parse input `shouldBe` [(value, input)]

    it "should return success when the first parser fails but the second succeeds" $
      let value = 42
          input = "abc"
          parse = failp `alt` succeedp value
       in parse input `shouldBe` [(value, input)]

    it "should return both success values when both parsers succeed" $
      let p1Value = 1
          p2Value = 2
          input = "abc"
          parse = succeedp p1Value `alt` succeedp p2Value
       in parse input `shouldBe` [(p1Value, input), (p2Value, input)]

    it "should fail when both parsers fail" $
      let parse = (failp `alt` failp) :: Parser Char Char
       in parse "abc" `shouldBe` []

    context "when one (or more) of the parsers returns multiple values" $ do
      it "should return all the successes when the first parser returns multiple results and the second parser fails" $
        let p1Value = 1
            p2Value = 2
            input = "abc"
            parse = succeedp p1Value `alt` succeedp p2Value `alt` failp
         in parse input `shouldBe` [(p1Value, input), (p2Value, input)]

      it "should return all the successes when the first parser fails but the second parser returns multiple results" $
        let p1Value = 1
            p2Value = 2
            input = "abc"
            parse = failp `alt` succeedp p1Value `alt` succeedp p2Value
         in parse input `shouldBe` [(p1Value, input), (p2Value, input)]

      it "should return all the successes when both parsers return multiple results" $
        let input = "abc"
            parse = succeedp 1 `alt` succeedp 2 `alt` succeedp 3 `alt` succeedp 4
         in parse input `shouldBe` [(1, input), (2, input), (3, input), (4, input)]

  describe "thenp" $ do
    it "should return success when both parsers succeed" $
      let p1Value = 1
          p2Value = 2
          input = "abc"
          parse = succeedp p1Value `thenp` succeedp p2Value
       in parse input `shouldBe` [((p1Value, p2Value), input)]

    it "should fail when the first parser succeeds but the second parser fails" $
      let parse = succeedp 42 `thenp` (failp :: Parser Char Char)
       in parse "abc" `shouldBe` []

    it "should fail when the first parser fails even though the second parser would succeed" $
      let parse = (failp :: Parser Char Char) `thenp` succeedp 42
       in parse "abc" `shouldBe` []

    it "should fail when both parsers fail" $
      let parse = (failp `thenp` failp) :: Parser Char (Char, Char)
       in parse "abc" `shouldBe` []

    -- example from the paper
    it "applying the parser (literal 'a' `thenp` literal 'b') to the input \"abcd\" gives the result [(('a','b'),\"cd\")]" $
      let parse = literal 'a' `thenp` literal 'b'
       in parse "abcd" `shouldBe` [(('a', 'b'), "cd")]

    context "when one (or more) of the parsers returns multiple values" $ do
      it "should return the cross-product of successes when the first parser returns multiple results and the second parser succeeds" $
        let input = "abc"
            parse = (succeedp 1 `alt` succeedp 2) `thenp` succeedp 'x'
         in parse input `shouldBe` [((1, 'x'), input), ((2, 'x'), input)]

      it "should return the cross-product of successes when the first parser succeeds and the second parser returns multiple results" $
        let input = "abc"
            parse = succeedp 'x' `thenp` (succeedp 1 `alt` succeedp 2)
         in parse input `shouldBe` [(('x', 1), input), (('x', 2), input)]

      it "should return the cross-product of successes when both parsers return multiple results" $
        let input = "abc"
            parse = (succeedp 1 `alt` succeedp 2) `thenp` (succeedp 'x' `alt` succeedp 'y')
         in parse input `shouldBe` [((1, 'x'), input), ((1, 'y'), input), ((2, 'x'), input), ((2, 'y'), input)]

      it "should fail when the first parser returns multiple results but the second parser fails" $
        let parse = (succeedp 1 `alt` succeedp 2) `thenp` (failp :: Parser Char Char)
         in parse "abc" `shouldBe` []