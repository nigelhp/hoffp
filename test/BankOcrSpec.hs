module BankOcrSpec (spec) where

import BankOcr (Cell (..), Digit (..), OcrEntry (..), PossibleDigit (..), accountNumberEntry, blankLine, eval, ocrEntry, validateChecksum)
import GHC.Generics (Associativity (LeftAssociative))
import Parser.Manipulation (many, thenx)
import Test.Hspec (Spec, describe, it, shouldBe)

spec :: Spec
spec = do
  describe "a digit can be printed in a friendly manner" $ do
    it "prints One as the numeral `1`" $
      show One `shouldBe` "1"

    it "prints Two as the numeral `2`" $
      show Two `shouldBe` "2"

    it "prints Three as the numeral `3`" $
      show Three `shouldBe` "3"

    it "prints Four as the numeral `4`" $
      show Four `shouldBe` "4"

    it "prints Five as the numeral `5`" $
      show Five `shouldBe` "5"

    it "prints Six as the numeral `6`" $
      show Six `shouldBe` "6"

    it "prints Seven as the numeral `7`" $
      show Seven `shouldBe` "7"

    it "prints Eight as the numeral `8`" $
      show Eight `shouldBe` "8"

    it "prints Nine as the numeral '9'" $
      show Nine `shouldBe` "9"

    it "prints Zero as the numeral '0'" $
      show Zero `shouldBe` "0"

  describe "a possible digit" $ do
    it "prints as the numeral value when legible" $
      show (LegibleDigit One) `shouldBe` "1"

    it "prints as '?' when illegible" $
      let badDigit = ((Empty, Empty, Empty), (Horizontal, Horizontal, Horizontal), (Vertical, Vertical, Vertical))
       in show (IllegibleDigit badDigit) `shouldBe` "?"

  describe "a parsed ocr entry can be printed in a friendly manner" $ do
    it "prints the digits of a legible entry as numerals" $
      let legibleEntry = LegibleEntry (One, Two, Three, Four, Five, Six, Seven, Eight, Nine)
       in show legibleEntry `shouldBe` "123456789"

    it "prints the legible digits of an illegible entry as numerals, and the illegible digits as '?'" $
      let badDigit = ((Empty, Empty, Empty), (Horizontal, Horizontal, Horizontal), (Vertical, Vertical, Vertical))
          illegibleEntry =
            IllegibleEntry
              ( LegibleDigit Eight,
                LegibleDigit Six,
                LegibleDigit One,
                LegibleDigit One,
                LegibleDigit Zero,
                IllegibleDigit badDigit,
                IllegibleDigit badDigit,
                LegibleDigit Three,
                LegibleDigit Six
              )
       in show illegibleEntry `shouldBe` "86110??36"

    {-
     - The first 3 lines of each entry contain an account number written using pipes and underscores.
     - Each line has 27 characters.
     - Each account number should have 9 digits.
     -}
    describe "the parser for an ocr entry" $ do
      it "succeeds with a legible entry when the input consists of the 3-line ocr representation of the account number '123456789'" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _ ",
                  "  | _| _||_||_ |_   ||_||_|",
                  "  ||_  _|  | _||_|  ||_| _|"
                ]
            expectedLegibleEntry = LegibleEntry (One, Two, Three, Four, Five, Six, Seven, Eight, Nine)
         in eval ocrEntry input `shouldBe` Just expectedLegibleEntry

      it "succeeds with a legible entry when the input consists of the 3-line ocr representation of the account number '876543210'" $
        let input =
              unlines
                [ " _  _  _  _     _  _     _ ",
                  "|_|  ||_ |_ |_| _| _|  || |",
                  "|_|  ||_| _|  | _||_   ||_|"
                ]
            expectedLegibleEntry = LegibleEntry (Eight, Seven, Six, Five, Four, Three, Two, One, Zero)
         in eval ocrEntry input `shouldBe` Just expectedLegibleEntry

      it "succeeds with an inelligible entry when an otherwise valid sequence of ' ', '|', and '_' do not match the representation of a digit" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _ ",
                  "  | _| _||_||_ |_   ||_||_ ",
                  "  ||_  _|  | _||_|  ||_||_ "
                ]
            expectedIllegibleEntry =
              IllegibleEntry
                ( LegibleDigit One,
                  LegibleDigit Two,
                  LegibleDigit Three,
                  LegibleDigit Four,
                  LegibleDigit Five,
                  LegibleDigit Six,
                  LegibleDigit Seven,
                  LegibleDigit Eight,
                  IllegibleDigit ((Empty, Horizontal, Empty), (Vertical, Horizontal, Empty), (Vertical, Horizontal, Empty))
                )
         in eval ocrEntry input `shouldBe` Just expectedIllegibleEntry

      it "fails when the input has fewer than 3 lines" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _ ",
                  "  | _| _||_||_ |_   ||_||_|"
                ]
         in eval ocrEntry input `shouldBe` Nothing

      it "fails when the input has more than 3 lines" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _ ",
                  "  | _| _||_||_ |_   ||_||_|",
                  "  ||_  _|  | _||_|  ||_| _|",
                  "                           "
                ]
         in eval ocrEntry input `shouldBe` Nothing

      it "fails when the input lines have fewer than 27 characters" $
        let input =
              unlines
                [ "   _  _     _  _  _  _  _ ",
                  " | _| _||_||_ |_   ||_||_|",
                  " ||_  _|  | _||_|  ||_| _|"
                ]
         in eval ocrEntry input `shouldBe` Nothing

      it "fails when the input lines have more than 27 characters" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _  ",
                  "  | _| _||_||_ |_   ||_||_| ",
                  "  ||_  _|  | _||_|  ||_| _| "
                ]
         in eval ocrEntry input `shouldBe` Nothing

      it "fails when all 3 lines are not terminated by a newline" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _ ",
                  "  | _| _||_||_ |_   ||_||_|"
                ]
                ++ "  ||_  _|  | _||_|  ||_| _|"
         in eval ocrEntry input `shouldBe` Nothing

      it "fails when the input lines contain a character other than ' ', '|' or '_'" $
        let input =
              unlines
                [ "    _  _     _  _  _  _  _ ",
                  " /| _| _||_||_ |_   ||_||_|",
                  "  ||_  _|  | _||_|  ||_| _|"
                ]
         in eval ocrEntry input `shouldBe` Nothing

  describe "the parser for a blank ocr line" $ do
    it "succeeds when the input line consists of 27 spaces" $
      let input = replicate 27 ' ' ++ ['\n']
       in eval blankLine input `shouldBe` Just ()

    it "fails when the input line consists of fewer than 27 spaces" $
      let input = replicate 26 ' ' ++ ['\n']
       in eval blankLine input `shouldBe` Nothing

    it "fails when the input line consists of more than 27 spaces" $
      let input = replicate 28 ' ' ++ ['\n']
       in eval blankLine input `shouldBe` Nothing

    it "fails when the input line contains a non-space character" $
      let input = replicate 26 ' ' ++ ['*', '\n']
       in eval blankLine input `shouldBe` Nothing

    it "fails when the 27 spaces are not terminated with a newline" $
      let input = replicate 27 ' '
       in eval blankLine input `shouldBe` Nothing

  describe "a parsed account number entry can be printed in a friendly manner" $ do
    it "prints a legible entry with a valid checksum as numeral digits" $
      let legibleEntry = LegibleEntry (Three, Four, Five, Eight, Eight, Two, Eight, Six, Five)
       in show (validateChecksum legibleEntry) `shouldBe` "345882865"

    it "prints a legible entry with an invalid checksum as numeral digits followed by the status code 'ERR'" $
      let legibleEntryWithBadChecksum = LegibleEntry (Six, Six, Four, Three, Seven, One, Four, Nine, Five)
       in show (validateChecksum legibleEntryWithBadChecksum) `shouldBe` "664371495 ERR"

    it "prints an illegible entry as numeral digits with '?' replacing illegible digits followed by the status code 'ILL'" $
      let badDigit = ((Empty, Empty, Empty), (Horizontal, Horizontal, Horizontal), (Vertical, Vertical, Vertical))
          illegibleEntry = IllegibleEntry (LegibleDigit Eight, LegibleDigit Six, LegibleDigit One, LegibleDigit One, LegibleDigit Zero, IllegibleDigit badDigit, IllegibleDigit badDigit, LegibleDigit Three, LegibleDigit Six)
       in show (validateChecksum illegibleEntry) `shouldBe` "86110??36 ILL"

  {-
   - The first 3 lines of each entry contain an account number written using pipes and underscores.
   - Each line has 27 characters.
   - Each account number should have 9 digits.
   - A valid account number requires a valid checksum.
   -}
  describe "the parser for an account number entry" $ do
    it "succeeds with an account number when the input consists of the 3-line ocr representation of the account number '457508000'" $
      let input =
            unlines
              [ "    _  _  _  _  _  _  _  _ ",
                "|_||_   ||_ | ||_|| || || |",
                "  | _|  | _||_||_||_||_||_|"
              ]
          maybeAccountNumber = eval accountNumberEntry input
       in fmap show maybeAccountNumber `shouldBe` Just "457508000"

    it "succeeds with a bad checksum when the input consists of the 3-line ocr representation of the account number '664371495'" $
      let input =
            unlines
              [ " _  _     _  _        _  _ ",
                "|_ |_ |_| _|  |  ||_||_||_ ",
                "|_||_|  | _|  |  |  | _| _|"
              ]
          maybeAccountNumber = eval accountNumberEntry input
       in fmap show maybeAccountNumber `shouldBe` Just "664371495 ERR"

    it "succeeds with an illegible entry when any of the digits of a valid 3-line ocr representation cannot be identified" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _ ",
                "  | _| _||_| _ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _ "
              ]
          maybeAccountNumber = eval accountNumberEntry input
       in fmap show maybeAccountNumber `shouldBe` Just "1234?678? ILL"

    it "fails when the input has fewer than 3 lines" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|"
              ]
       in eval accountNumberEntry input `shouldBe` Nothing

    it "fails when the input has more than 3 lines" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _|",
                "                           "
              ]
       in eval accountNumberEntry input `shouldBe` Nothing

    it "fails when the input lines have fewer than 27 characters" $
      let input =
            unlines
              [ "   _  _     _  _  _  _  _ ",
                " | _| _||_||_ |_   ||_||_|",
                " ||_  _|  | _||_|  ||_| _|"
              ]
       in eval accountNumberEntry input `shouldBe` Nothing

    it "fails when the input lines have more than 27 characters" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _  ",
                "  | _| _||_||_ |_   ||_||_| ",
                "  ||_  _|  | _||_|  ||_| _| "
              ]
       in eval accountNumberEntry input `shouldBe` Nothing

    it "fails when all 3 lines are not terminated by a newline" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|"
              ]
              ++ "  ||_  _|  | _||_|  ||_| _|"
       in eval accountNumberEntry input `shouldBe` Nothing

    it "fails when the input lines contain a character other than ' ', '|' or '_'" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _ ",
                " /| _| _||_||_ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _|"
              ]
       in eval accountNumberEntry input `shouldBe` Nothing

  {-
   - Each entry is 4 lines long, and each line has 27 characters.
   - The first 3 lines of each entry contain an account number written using pipes and underscores, and the fourth line is blank.
   - Each account number should have 9 digits.
   -}
  describe "a parser for an ocr file can be composed from the 'many', 'accountNumberEntry' and 'blankLine' parsers" $ do
    it "succeeds when the input contains many entries complying with the rules for an ocr entry" $
      let input =
            unlines
              [ " _  _  _  _  _  _  _  _  _ ",
                "| || || || || || || || || |",
                "|_||_||_||_||_||_||_||_||_|",
                "                           ",
                "                           ",
                "  |  |  |  |  |  |  |  |  |",
                "  |  |  |  |  |  |  |  |  |",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                " _| _| _| _| _| _| _| _| _|",
                "|_ |_ |_ |_ |_ |_ |_ |_ |_ ",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                " _| _| _| _| _| _| _| _| _|",
                " _| _| _| _| _| _| _| _| _|",
                "                           ",
                "                           ",
                "|_||_||_||_||_||_||_||_||_|",
                "  |  |  |  |  |  |  |  |  |",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                "|_ |_ |_ |_ |_ |_ |_ |_ |_ ",
                " _| _| _| _| _| _| _| _| _|",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                "|_ |_ |_ |_ |_ |_ |_ |_ |_ ",
                "|_||_||_||_||_||_||_||_||_|",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                "  |  |  |  |  |  |  |  |  |",
                "  |  |  |  |  |  |  |  |  |",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                "|_||_||_||_||_||_||_||_||_|",
                "|_||_||_||_||_||_||_||_||_|",
                "                           ",
                " _  _  _  _  _  _  _  _  _ ",
                "|_||_||_||_||_||_||_||_||_|",
                " _| _| _| _| _| _| _| _| _|",
                "                           ",
                "    _  _     _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _|",
                "                           ",
                " _  _  _  _  _     _  _    ",
                "|_||_|  ||_ |_ |_| _| _|  |",
                " _||_|  ||_| _|  | _||_   |",
                "                           ",
                "    _  _  _  _  _  _  _  _ ",
                "|_||_   ||_ | ||_|| || || |",
                "  | _|  | _||_||_||_||_||_|",
                "                           ",
                " _  _     _  _        _  _ ",
                "|_ |_ |_| _|  |  ||_||_||_ ",
                "|_||_|  | _|  |  |  | _| _|",
                "                           ",
                " _  _        _  _  _  _  _ ",
                "|_||_   |  || | _| _  _||_ ",
                "|_||_|  |  ||_| _  _| _||_|",
                "                           ",
                " _  _  _  _  _  _  _  _    ",
                "| || || || || || || ||_   |",
                "|_||_||_||_||_||_||_| _|  |",
                "                           ",
                "    _  _  _  _  _  _     _ ",
                "|_||_|| || ||_   |  |  | _ ",
                "  | _||_||_||_|  |  |  | _|",
                "                           ",
                "    _  _     _  _  _  _  _ ",
                "  | _| _||_| _ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _ ",
                "                           "
              ]
          expectedEntries =
            [ "000000000",
              "111111111 ERR",
              "222222222 ERR",
              "333333333 ERR",
              "444444444 ERR",
              "555555555 ERR",
              "666666666 ERR",
              "777777777 ERR",
              "888888888 ERR",
              "999999999 ERR",
              "123456789",
              "987654321 ERR",
              "457508000",
              "664371495 ERR",
              "86110??36 ILL",
              "000000051",
              "49006771? ILL",
              "1234?678? ILL"
            ]
          entryParser = accountNumberEntry `thenx` blankLine
          fileParser = many entryParser
          parseResult = eval fileParser input
       in fmap (map show) parseResult `shouldBe` Just expectedEntries

    it "succeeds when the input contains one entry complying with the rules for an ocr entry" $
      let input =
            unlines
              [ "    _  _     _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _|",
                "                           "
              ]
          entryParser = accountNumberEntry `thenx` blankLine
          fileParser = many entryParser
          parseResult = eval fileParser input
       in fmap (map show) parseResult `shouldBe` Just ["123456789"]

    -- by contrast, use 'some' rather than 'many' to enforce at least one account number
    it "succeeds when the input contains no entries" $
      let emptyInput = ""
          entryParser = accountNumberEntry `thenx` blankLine
          fileParser = many entryParser
       in eval fileParser emptyInput `shouldBe` Just []