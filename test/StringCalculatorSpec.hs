module StringCalculatorSpec (spec) where

import StringCalculator (StringCalculatorError (..), add)
import Test.Hspec (Spec, context, describe, it, shouldBe)

spec :: Spec
spec = do
  describe "add" $ do
    describe "when a custom delimiter is not defined" $ do
      it "returns zero when the input string is empty" $
        add "" `shouldBe` Right 0

      it "returns the number when the input string is a single number e.g. '1'" $
        add "1" `shouldBe` Right 1

      it "returns the sum when the input string is two numbers separated by a comma e.g. '1,2'" $
        add "1,2" `shouldBe` Right 3

      it "returns the sum when the input string is two numbers separated by a newline e.g. '1\n2'" $
        add "1\n2" `shouldBe` Right 3

      it "returns the sum when the input string is three numbers separated by a comma e.g. '1,2,3'" $
        add "1,2,3" `shouldBe` Right 6

      it "returns the sum when the input string is three numbers separated by a newline e.g. '1\n2\n3'" $
        add "1\n2\n3" `shouldBe` Right 6

      it "returns the sum when the input string is three numbers with the first pair separated by a newline and the second pair separated by a comma e.g. '1\n2,3'" $
        add "1\n2,3" `shouldBe` Right 6

      it "returns the sum when the input string is three numbers with the first pair separated by a comman and the second pair separated by a newline eg. '1,2\n3'" $
        add "1,2\n3" `shouldBe` Right 6

      it "returns the sum when the input string contains a sequence of numbers separated by commas or newlines" $
        add "1,2\n3,4\n5,6\n7,8\n9" `shouldBe` Right 45

      context "when the input string is invalid" $ do
        it "returns a parse error when the input string is a single non-numeric value e.g. 'a'" $
          add "a" `shouldBe` Left ParseError

        it "returns a parse error when the input string is two non-numeric values separated by a comma e.g. 'a,b'" $
          add "a,b" `shouldBe` Left ParseError

        it "returns a parse error when the input string is two non-numeric values separated by a newline e.g. 'a\nb'" $
          add "a\nb" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a number and a non-numeric value separated by a comma e.g. '1,a'" $
          add "1,a" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a number and a non-numeric value separated by a newline e.g. '1\na'" $
          add "1\na" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a non-numeric value and a number separated by a comma e.g. 'a,1'" $
          add "a,1" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a non-numeric value and a number separated by a newline e.g. 'a\n1'" $
          add "a\n1" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a number followed by a comma e.g. '1,'" $
          add "1," `shouldBe` Left ParseError

        it "returns a parse error when the input string is a number followed by a newline e.g. '1\n'" $
          add "1\n" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a number followed by a comma and a newline e.g. '1,\n'" $
          add "1,\n" `shouldBe` Left ParseError

        it "returns a parse error when the input string contains comma separated numbers followed by a trailing comma e.g. '1,2,'" $
          add "1,2," `shouldBe` Left ParseError

        it "returns a parse error when the input string contains newline separated numbers followed by a trailing newline e.g. '1\n2\n'" $
          add "1\n2\n" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a comma followed by a number e.g. ',1'" $
          add ",1" `shouldBe` Left ParseError

        it "returns a parse error when the input string is a newline followed by a number e.g. '\n1'" $
          add "\n1" `shouldBe` Left ParseError

        it "returns a parse error when the input string is two numbers separated by two commas e.g. '1,,2'" $
          add "1,,2" `shouldBe` Left ParseError

        it "returns a parse error when the input string is two numbers separated by two newlines e.g. '1\n\n2'" $
          add "1\n\n2" `shouldBe` Left ParseError

        it "returns a parse error when the input string is two numbers separated by both a comma and a newline e.g. '1,\n2'" $
          add "1,\n2" `shouldBe` Left ParseError

        it "returns a parse error when the input string is two numbers separated by a semicolon (rather than a comma or newline) e.g. '1;2'" $
          add "1;2" `shouldBe` Left ParseError

    describe "when a single character custom delimiter is defined" $ do
      it "returns zero when the delimiter definition is followed by an empty sequence e.g. '//;\n'" $
        add "//;\n" `shouldBe` Right 0

      it "returns the number when the delimiter definition is followed by a single number e.g. '//;\n1'" $
        add "//;\n1" `shouldBe` Right 1

      it "returns the sum when the delimiter definition is followed by a sequence of numbers separated by that delimiter e.g. '//;\n1;2'" $
        add "//;\n1;2" `shouldBe` Right 3

      it "supports a delimiter of '/' (the character itself used to define a custom delimiter) e.g. '///\n1/2'" $
        add "///\n1/2" `shouldBe` Right 3

      it "supports a delimiter of '-' (a character which otherwise would be interpreted as minus sign)" $
        add "//-\n1-2" `shouldBe` Right 3

      it "supports a delimiter of '[' (a character used to start a multi-character custom delimiter) e.g. '//[\n1[2'" $
        add "//[\n1[2" `shouldBe` Right 3

      it "supports a delimiter of ']' (a character used to end a multi-character custom delimiter) e.g. '//]\n1]2'" $
        add "//]\n1]2" `shouldBe` Right 3

      context "when the input string is invalid" $ do
        it "returns a parse error when the sequence of numbers is not separated by the defined delimiter e.g. '//;\n1,2'" $
          add "//;\n1,2" `shouldBe` Left ParseError

        it "returns a parse error the input string attempts to define a delimiter consisting of multiple characters e.g. '//**\n1**2'" $
          add "//**\n1**2" `shouldBe` Left ParseError

        it "returns a parse error when the input string does not end the delimiter definition with a newline" $
          add "//;" `shouldBe` Left ParseError

        it "returns a parse error when the input string starts with '//' and ends with a newline but does not define a delimiter" $
          add "//\n" `shouldBe` Left ParseError

        it "returns a parse error when the input string starts with '//' but does not define a delimiter" $
          add "//" `shouldBe` Left ParseError

    describe "when a multi character custom delimiter is defined" $ do
      it "returns zero when the delimiter definition is followed by an empty sequence e.g. '//[***]\n'" $
        add "//[***]\n" `shouldBe` Right 0

      it "returns the number when the delimiter definition is followed by a single number e.g. '//[***]\n1'" $
        add "//[***]\n1" `shouldBe` Right 1

      it "returns the sum when the delimiter definition is followed by a sequence of numbers separated by that delimiter e.g. '//[***]\n1***2***3'" $
        add "//[***]\n1***2***3" `shouldBe` Right 6

      it "supports a delimiter of '//' (which is itself used to define a custom delimiter) e.g. ''" $
        add "//[//]\n1//2//3" `shouldBe` Right 6

      it "supports a delimiter of '--' (with any number in such a sequence not being interpreted as negative numbers) e.g. '//[--]\n1--2--3'" $
        add "//[--]\n1--2--3" `shouldBe` Right 6

      it "supports a delimiter of '[[[' (which contains the '[' character used to start a multi-character custom delimiter)" $
        add "//[[[[]\n1[[[2[[[3" `shouldBe` Right 6

      it "supports a delimiter of ']]' (which contains the ']' character used to end a multi-character custom delimiter)" $
        add "//[]]]\n1]]2]]3" `shouldBe` Right 6

      context "when the input string is invalid" $ do
        it "returns a parse error when the sequence of numbers is not separated by the defined delimiter e.g. '//[***]\n1,2,3'" $
          add "//[***]\n1,2,3" `shouldBe` Left ParseError

        it "returns a parse error when the delimiter definition is empty e.g. '//[]\n'" $
          add "//[]\n" `shouldBe` Left ParseError

        it "returns a parse error when the input string does not close the delimiter definition" $
          add "//[***\n1" `shouldBe` Left ParseError

        it "returns a parse error when the input string does not end the delimiter definition with a newline" $
          add "//[***]1" `shouldBe` Left ParseError

    describe "when many custom delimiters are defined" $ do
      context "when single character delimiters" $ do
        it "returns zero when the delimiter definitions are followed by an empty sequence e.g. '//[*][%]\n'" $
          add "//[*][%]\n" `shouldBe` Right 0

        it "returns the number when the delimiter definitions are followed by a single number e.g. '//[*][%]\n1'" $
          add "//[*][%]\n1" `shouldBe` Right 1

        it "returns the sum when the delimiter definitions are followed by a sequence of numbers separated by any of those delimiters e.g. '//[*][%]\n1*2%3'" $
          add "//[*][%]\n1*2%3" `shouldBe` Right 6

        it "supports a delimiter of '/' (a character used in the definition of a custom delimiter)" $
          add "//[*][/]\n1*2/3" `shouldBe` Right 6

        it "supports a delimiter of '-' (with any numbers so delimited not being interpreted as negative numbers)" $
          add "//[*][-]\n1*2-3" `shouldBe` Right 6

        it "supports a delimiter of '[' (the character used to start a multi-character custom delimiter)" $
          add "//[*][[]\n1*2[3" `shouldBe` Right 6

      context "when multi character delimiters" $ do
        it "returns zero when the delimiter definitions are followed by an empty sequence e.g. '//[***][%%%]\n'" $
          add "//[***][%%%]\n" `shouldBe` Right 0

        it "returns the number when the delimiter definitions are followed by a single number e.g. '//[***][%%%]\n1'" $
          add "//[***][%%%]\n1" `shouldBe` Right 1

        it "returns the sum when the delimiter definitions are followed by a sequence of numbers separated by any of those delimiters e.g. '//[***][%%%]\n1***2%%%3'" $
          add "//[***][%%%]\n1***2%%%3" `shouldBe` Right 6

        it "supports a delimiter definition of '//' (which is itself used to define a custom delimiter)" $
          add "//[***][//]\n1***2//3" `shouldBe` Right 6

        it "supports a delimiter definition of '--' (with any number in such a sequence not being interpreted as negative numbers)" $
          add "//[***][--]\n1***2--3" `shouldBe` Right 6

        it "supports a delimiter that contains '[' (the character used to start a multi-character custom delimiter)" $
          add "//[**[][[[[]\n1**[2[[[3" `shouldBe` Right 6

        it "supports a delimiter that contains a ']' (the character used to end a multi-character custom delimiter)" $
          add "//[*][]]\n1*2]3" `shouldBe` Right 6

      context "when the input string is invalid" $ do
        it "returns a parse error when the sequence of numbers is not separated by any of the defined delimiters e.g. ''" $
          add "//[*][%]\n1,2,3" `shouldBe` Left ParseError

        it "returns a parse error when a delimiter definition is empty e.g. '//[*][]\n1'" $
          add "//[*][]\n1" `shouldBe` Left ParseError

        it "returns a parse error when the delimiter definitions are separated by an unexpected delimiter e.g. ','" $
          add "//[*],[%]\n1*2%3" `shouldBe` Left ParseError

        it "returns a parse error when the input string does not close the final delimiter definition" $
          add "//[*][%\n1" `shouldBe` Left ParseError

        it "returns a parse error when the input string does not end the delimiter definitions with a newline" $
          add "//[*][%]1" `shouldBe` Left ParseError

    describe "returns a negatives not allowed error" $ do
      context "when a custom delimiter is not defined" $ do
        it "when the input string is a single negative number" $
          add "-1" `shouldBe` Left (NegativesNotAllowedError [-1])

        it "when the input string contains multiple negative numbers amidst many positive numbers" $
          add "1,2,-3,4,-5" `shouldBe` Left (NegativesNotAllowedError [-3, -5])

      context "when a single character custom delimiter is defined" $ do
        it "when the input string is a single negative number" $
          add "//;\n-1" `shouldBe` Left (NegativesNotAllowedError [-1])

        it "when the input string contains multiple negative numbers amidst many positive numbers" $
          add "//;\n1;2;-3;4;-5" `shouldBe` Left (NegativesNotAllowedError [-3, -5])

      context "when a multi character custom delimiter is defined" $ do
        it "when the input string is a single negative number" $
          add "//[***]\n-1" `shouldBe` Left (NegativesNotAllowedError [-1])

        it "when the input string contains multiple negative numbers amidst many positive numbers" $
          add "//[***]\n1***2***-3***4***-5" `shouldBe` Left (NegativesNotAllowedError [-3, -5])

      context "when many custom delimiters are defined" $ do
        it "when the input string is a single negative number" $
          add "//[*][%]\n-1" `shouldBe` Left (NegativesNotAllowedError [-1])

        it "when the input string contains multiple negative numbers amidst many positive numbers" $
          add "//[*][%]\n1*2%-3*4%-5" `shouldBe` Left (NegativesNotAllowedError [-3, -5])

    describe "numbers bigger than 1000 should be silently ignored" $ do
      context "when a custom delimiter is not defined" $ do
        it "includes within the sum numbers up to and including 1000" $
          add "1,1000" `shouldBe` Right 1001

        it "silently ignores a number greater than 1000 when the input is a single number" $
          add "1001" `shouldBe` Right 0

        it "silently ignores a number greater than 1000 when the input is a valid sequence of numbers" $
          add "2,1001" `shouldBe` Right 2

      context "when a single character custom delimiter is defined" $ do
        it "includes within the sum numbers up to and including 1000" $
          add "//;\n1;1000" `shouldBe` Right 1001

        it "silently ignores a number greater than 1000 when the input is a single number" $
          add "//;\n1001" `shouldBe` Right 0

        it "silently ignores a number greater than 1000 when the input is a valid sequence of numbers" $
          add "//;\n2;1001" `shouldBe` Right 2

      context "when a multi character custom delimiter is defined" $ do
        it "includes within the sum numbers up to and including 1000" $
          add "//[***]\n1***1000" `shouldBe` Right 1001

        it "silently ignores a number greater than 1000 when the input is a single number" $
          add "//[***]\n1001" `shouldBe` Right 0

        it "silently ignores a number greater than 1000 when the input is a valid sequence of numbers" $
          add "//[***]\n2***1001" `shouldBe` Right 2

      context "when many custom delimiters are defined" $ do
        it "includes within the sum numbers up to and including 1000" $
          add "//[*][%]\n1*1000" `shouldBe` Right 1001

        it "silently ignores a number greater than 1000 when the input is a single number" $
          add "//[*][%]\n1001" `shouldBe` Right 0

        it "silently ignores a number greater than 1000 when the input is a valid sequence of numbers" $
          add "//[*][%]\n2*1001" `shouldBe` Right 2
