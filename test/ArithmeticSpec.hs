module ArithmeticSpec (spec) where

import Arithmetic (Expr (..), expn, factor, term)
import Test.Hspec (Spec, describe, it, shouldBe)

spec :: Spec
spec = do
  describe "factor" $ do
    it "can be a single digit number" $
      let result = factor "1"
       in result `shouldBe` [(Num 1, "")]

    it "can be a multiple digit number" $
      let result = factor "123"
       in result `shouldBe` [(Num 123, ""), (Num 12, "3"), (Num 1, "23")]

    it "can be a parenthesized expression" $
      let result = factor "(2+3)"
       in result `shouldBe` [(Num 2 `Add` Num 3, "")]

    it "can be a parenthesized term" $
      let result = factor "(2*10)"
       in result `shouldBe` [(Num 2 `Mul` Num 10, "")]

    it "can be a parenthesized factor" $
      let result = factor "(123)"
       in result `shouldBe` [(Num 123, "")]

  describe "term" $ do
    it "can be a multiplication" $
      let result = term "2*3"
       in result `shouldBe` [(Num 2 `Mul` Num 3, ""), (Num 2, "*3")]

    it "can be a division" $
      let result = term "1/5"
       in result `shouldBe` [(Num 1 `Div` Num 5, ""), (Num 1, "/5")]

    it "can be a single digit factor" $
      let result = term "1"
       in result `shouldBe` [(Num 1, "")]

    it "can be a multiple digit factor" $
      let result = term "123"
       in result `shouldBe` [(Num 123, ""), (Num 12, "3"), (Num 1, "23")]

    it "can be a parenthesized expression" $
      let result = term "(2+3)"
       in result `shouldBe` [(Num 2 `Add` Num 3, "")]

    it "can be a parenthesized term" $
      let result = term "(2*10)"
       in result `shouldBe` [(Num 2 `Mul` Num 10, "")]

    it "can be a parenthesized factor" $
      let result = factor "(123)"
       in result `shouldBe` [(Num 123, "")]

  describe "expression" $ do
    it "can be an addition" $
      let result = expn "2+3"
       in result `shouldBe` [(Num 2 `Add` Num 3, ""), (Num 2, "+3")]

    it "can be a subtraction" $
      let result = expn "7-4"
       in result `shouldBe` [(Num 7 `Sub` Num 4, ""), (Num 7, "-4")]

    it "can be a multiplication term" $
      let result = expn "2*3"
       in result `shouldBe` [(Num 2 `Mul` Num 3, ""), (Num 2, "*3")]

    it "can be a single digit factor" $
      let result = expn "1"
       in result `shouldBe` [(Num 1, "")]

    it "can be a multiple digit factor" $
      let result = expn "123"
       in result `shouldBe` [(Num 123, ""), (Num 12, "3"), (Num 1, "23")]

    it "can be a parenthesized expression" $
      let result = expn "(2+3)"
       in result `shouldBe` [(Num 2 `Add` Num 3, "")]

    it "can be a parethesized term" $
      let result = expn "(2*10)"
       in result `shouldBe` [(Num 2 `Mul` Num 10, "")]

    it "can be a parenthesized factor" $
      let result = expn "(123)"
       in result `shouldBe` [(Num 123, "")]

    -- example from the paper
    it "can parse \"2+(4-1)*3\"" $
      let result = expn "2+(4-1)*3"
       in result
            `shouldBe` [ (Add (Num 2) (Mul (Sub (Num 4) (Num 1)) (Num 3)), ""),
                         (Add (Num 2) (Sub (Num 4) (Num 1)), "*3"),
                         (Num 2, "+(4-1)*3")
                       ]