FROM haskell:8.10.4

WORKDIR /usr/src/hoffp

# Add just those files that define dependencies
COPY ./stack.yaml /usr/src/hoffp/stack.yaml
COPY ./stack.yaml.lock /usr/src/hoffp/stack.yaml.lock
COPY ./package.yaml /usr/src/hoffp/package.yaml

# Docker will cache this command as a layer, freeing us up to
# modify source code without re-installing dependencies
RUN stack test --only-dependencies
